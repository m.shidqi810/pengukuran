    <!-- Sidebar -->
    <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

        <!-- Sidebar - Brand -->
        <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.html">
            <div class="sidebar-brand-icon rotate-n-15">
                <i class="fas fa-hdd"></i>
            </div>
            <div class="sidebar-brand-text mx-3">Pengukuran</div>
        </a>

        <!-- Divider -->
        <hr class="sidebar-divider my-0">

        <!-- Nav Item - Dashboard -->
        <li class="nav-item @yield('dashboard')">
            <a class="nav-link" href="{{ route('dashboard') }}">
                <i class="fas fa-fw fa-tachometer-alt"></i>
                <span>Dashboard</span></a>
        </li>

        <!-- Divider -->
        <hr class="sidebar-divider">

        <!-- Heading -->
        <div class="sidebar-heading">
            Data
        </div>

        <li class="nav-item @yield('tekanan')">
            <a class="nav-link" href="{{ route('tekanan') }}">
                <i class="fas fa-server"></i>
                <span>Tekanan Udara</span></a>
        </li>
        
        <li class="nav-item @yield('about')">
            <a class="nav-link" href="{{ route('about') }}">
                <i class="fas fa-graduation-cap"></i>
                <span>About</span></a>
        </li>

        <!-- Divider -->
        <hr class="sidebar-divider">

        {{-- @if (Auth::user()->role == 'Dekanat')
            <li class="nav-item @yield('akun')">
                <a class="nav-link" href="{{ route('akun') }}">
                    <i class="fas fa-user"></i>
                    <span>Akun</span></a>
            </li>
        @endif

        <!-- Nav Item - Tables -->
        <li class="nav-item @yield('profile')">
            <a class="nav-link" href="{{ route('profile', Auth::user()->id) }}">
                <i class="fas fa-user-cog"></i>
                <span>Seting Profile</span></a>
        </li> --}}

        <!-- Sidebar Toggler (Sidebar) -->
        <div class="text-center d-none d-md-inline">
            <button class="rounded-circle border-0" id="sidebarToggle"></button>
        </div>

    </ul>
    <!-- End of Sidebar -->