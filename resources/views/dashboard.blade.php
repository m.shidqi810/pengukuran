@extends('layouts.app')
@section('judul', 'Dashboard')
@section('dashboard', 'active')

@section('isi')
    <!-- Content Row -->
    <div class="row">
        <div class="mx-auto col-md-8">
            <!-- Bar Chart -->
            <div class="card shadow mb-4">
                <div class="card-header text-center py-3">
                    <h6 class="m-0 font-weight-bold text-primary">Waktu Pengukuran</h6>
                    <h6>{{ $tekanan->waktu }}</h6>
                </div>
                <div class="card-body">
                    <div class="row text-center">
                        <div class="col-sm-6">
                            BMP280_1
                            <br>
                            <h2>
                                {{ $tekanan->ps1 }}
                            </h2>
                            hPa
                        </div>
                        <div class="col-sm-6">
                            BMP280_2
                            <br>
                            <h2>
                                {{ $tekanan->ps2 }}
                            </h2>
                            hPa
                        </div>
                    </div>
                </div>

                <div class="card-footer text-center">
                    Kondisi Sensor
                    <div class="row">
                        <div class="col-sm-6">
                            BMP280_1
                            <br>
                            <h2>
                                {{ $tekanan->rt1 == 1 ? 'OK' : 'Error' }}
                            </h2>
                        </div>
                        <div class="col-sm-6">
                            BMP280_2
                            <br>
                            <h2>
                                {{ $tekanan->rt2 == 1 ? 'OK' : 'Error' }}
                            </h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

{{-- @section('script')
    <script>
        var dataGuru = {
            labels: ["Fold 1", "Fold 2", "Fold 3", "Fold 4", "Fold 5"],
            datasets: [{
                label: 'Akurasi Total',
                data: [{{ $akurasi_chart[1]->akurasi_total }}, {{ $akurasi_chart[2]->akurasi_total }}, {{ $akurasi_chart[3]->akurasi_total }}, {{ $akurasi_chart[4]->akurasi_total }}, {{ $akurasi_chart[5]->akurasi_total }} ],
                backgroundColor: [
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 206, 86, 0.2)',
                    'rgba(75, 192, 192, 0.2)',
                ],
                borderColor: [
                    'rgba(255,99,132,1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(255, 206, 86, 1)',
                ],
                borderWidth: 1
            }]
        };
        var options = {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero:true
                        }
                    }]
                },
                legend: {
                    display: false
                },
                elements: {
                    point: {
                        radius: 3
                    }
                }

        };
        if($("#foldChart").length) {
        var foldChartCanvas = $("#foldChart").get(0).getContext("2d");
        var foldChart = new Chart(foldChartCanvas, {
            type: 'bar',
            data: dataGuru,
            options: options
        });
    }
    </script>
@endsection --}}