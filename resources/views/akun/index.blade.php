@extends('layouts.app')
@section('judul', 'Kelola Akun')
@section('akun', 'active')

@section('isi')
    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <a class="btn btn-sm btn-primary" href="{{ route('akun.tambah') }}">
                <i class="fas fa-plus text-white-50"></i> Tambah Data</a>
        </div>

        @each('akun.modal', $list_user, 'user')
        @each('akun.modal-delete', $list_user, 'user')

        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>Nama</th>
                            <th>Email</th>
                            <th>Role</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($list_user as $user)
                            <tr>
                                <td>{{ $user->name }}</td>
                                <td>{{ $user->email }}</td>
                                <td>{{ $user->role }}</td>
                                <td>
                                    <a href="{{ route('akun.edit', $user->id) }}" class="btn btn-sm btn-success"><i class="fas fa-edit"></i></a>
                                    <button class="btn btn-sm btn-danger" data-toggle="modal" data-target="#hapus{{$user->id}}"><i class="fas fa-trash"></i></button>
                                    <button class="btn btn-sm btn-info" data-toggle="modal" data-target="#detail{{$user->id}}"><i class="fas fa-search"></i></button>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>Nama</th>
                            <th>Email</th>
                            <th>Role</th>
                            <th>Action</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>


    <div class="modal modal-default fade" id="detail" data-focus="true" data-backdrop="true" data-keyboard="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="title-form"><b>Detail</b></h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label>NIM</label>
                        <input type="text" class="form-control" readonly
                        value="detail">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary pull-right" data-dismiss="modal">Tutup</button>
                </div>
            </div>
        </div>
    </div>


    <div class="modal modal-default fade" id="hapus" data-focus="true" data-backdrop="true" data-keyboard="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="title-form"><b>Konfirmasi Hapus</b></h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <h6>Yakin ingin menghapus data ini?</h6>
                    <h6>Data yang telah di hapus tidak akan bisa di pulihkan kembali!</h6>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary pull-right" data-dismiss="modal">Tutup</button>
                    <a href="" type="button" class="btn btn-danger pull-right">Hapus</a>
                </div>
            </div>
        </div>
    </div>
@endsection