<div class="modal modal-default fade" id="detail{{$user->id}}" data-focus="true" data-backdrop="true" data-keyboard="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="title-form"><b>Detail Akun</b></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label>Nama</label>
                    <input type="text" class="form-control" readonly
                    value="{{ $user->name }}">
                </div>
                <div class="form-group">
                    <label>Email</label>
                    <input type="email" class="form-control" readonly
                    value="{{ $user->email }}">
                </div>
                <div class="form-group">
                    <label>Role</label>
                    <input type="text" class="form-control" readonly
                    value="{{ $user->role }}">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary pull-right" data-dismiss="modal">Tutup</button>
            </div>
        </div>
    </div>
</div>