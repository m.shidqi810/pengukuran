@extends('layouts.app')
@section('judul', 'Kelola Akun')
@section('akun', 'active')

@section('isi')
    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-body">
            <form method="POST" id="form-tambah" action="{{ isset($akun) ? route('akun.update', $akun->id) : route('akun.simpan') }}" enctype="multipart/form-data">
                @csrf
                <div class="mb-3">
                    <label for="name" class="form-label">Nama</label>
                    <input type="text" class="form-control" id="name" name="name"
                    value="{{ isset($akun) ? $akun->name : old('akun') }}">
                </div>
                <div class="mb-3">
                    <label for="email" class="form-label">Email</label>
                    <input type="text" class="form-control" id="email" name="email"
                    value="{{ isset($akun) ? $akun->email : old('akun') }}">
                </div>
                <div class="mb-3">
                    <label for="password" class="form-label">Password</label>
                    <input type="password" class="form-control" id="password" name="password">
                    @if (isset($akun))
                        <p>*Kosongkan jika tidak ingin mengubah password</p>
                    @endif
                </div>
                
                <div class="mb-3">
                    <label for="role" class="form-label">Roles</label>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="role" id="role" value="Dekanat"
                        {{ isset($akun) ? ($akun->role == 'Dekanat' ? 'checked' : '' ) : '' }}>
                        <label class="form-check-label" for="role">
                            Dekanat
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="role" id="role" value="Ketua Program Studi"
                        {{ isset($akun) ? ($akun->role == 'Ketua Program Studi' ? 'checked' : '' ) : '' }}>
                        <label class="form-check-label" for="role">
                            Ketua Program Studi
                        </label>
                    </div>
                </div>

                <button type="submit" class="btn btn-primary">Simpan</button>

            </form>
        </div>
    </div>
@endsection