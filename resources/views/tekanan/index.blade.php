@extends('layouts.app')
@section('judul', 'Tekanan Udara')
@section('tekanan', 'active')

@section('isi')
    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <a href="{{ route('tekanan.tambah') }}" class="btn btn-sm btn-primary">
                <i class="fas fa-plus text-white-50"></i> Tambah Data</a>
        </div>

        @each('tekanan.modal', $daftar_tekanan, 'tekanan')
        @each('tekanan.modal-delete', $daftar_tekanan, 'tekanan')
        
        <div class="card-body">
            <div class="table-responsive mt-3">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>No.</th>
                            <th>Waktu Pengukuran</th>
                            <th>Tekanan Udara</th>
                            <th>Tekanan Udara</th>
                            <th>Kondisi</th>
                            <th>Kondisi</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @php $no = 1; @endphp
                        @foreach ($daftar_tekanan as $tekanan)    
                            <tr>
                                <td>{{ $no++ }}</td>
                                <td>{{ $tekanan->waktu }}</td>
                                <td>{{ $tekanan->ps1 }}</td>
                                <td>{{ $tekanan->ps2 }}</td>
                                <td>{{ $tekanan->rt1 }}</td>
                                <td>{{ $tekanan->rt2 }}</td>
                                <td>
                                    <a href="{{ route('tekanan.edit', $tekanan->id) }}" class="btn btn-sm btn-success"><i class="fas fa-edit"></i></a>
                                    <button class="btn btn-sm btn-danger" data-toggle="modal" data-target="#hapus{{$tekanan->id}}"><i class="fas fa-trash"></i></button>
                                    <button class="btn btn-sm btn-info" data-toggle="modal" data-target="#detail{{$tekanan->id}}"><i class="fas fa-search"></i></button>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection