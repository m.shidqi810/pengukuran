@extends('layouts.app')
@section('judul', 'Tekanan Udara')
@section('tekanan', 'active')

@section('isi')

    <a class="btn btn-primary btn-xs" href="{{ route('tekanan') }}"><i class="fa fa-arrow-left"></i> Kembali</a>

    <!-- DataTales Example -->
    <div class="card shadow mb-4 mt-4">
        <div class="card-body">
            <form method="POST" id="form-tambah" action="{{ isset($tekanan) ? route('tekanan.update', $tekanan->id) : route('tekanan.simpan') }}" enctype="multipart/form-data">
                @csrf

                <div class="mb-3">
                    <label for="waktu" class="form-label">Waktu Pengukuran</label>
                    <input type="date" class="form-control" id="waktu" name="waktu"
                    value="{{ isset($tekanan) ? $tekanan->waktu : old('waktu') }}">
                </div>

                <div class="mb-3">
                    <label for="ps1" class="form-label">BMP280_1</label>
                    <input type="text" class="form-control" id="ps1" name="ps1"
                    value="{{ isset($tekanan) ? $tekanan->ps1 : old('ps1') }}">
                </div>

                <div class="mb-3">
                    <label for="ps2" class="form-label">BMP280_2</label>
                    <input type="text" class="form-control" id="ps2" name="ps2"
                    value="{{ isset($tekanan) ? $tekanan->ps2 : old('ps2') }}">
                </div>
                
                <div class="mb-3">
                    <label for="rt1" class="form-label">BMP280_1</label>
                    <input type="text" class="form-control" id="rt1" name="rt1"
                    value="{{ isset($tekanan) ? $tekanan->rt1 : old('rt1') }}">
                </div>
                
                <div class="mb-3">
                    <label for="rt2" class="form-label">BMP280_2</label>
                    <input type="text" class="form-control" id="rt2" name="rt2"
                    value="{{ isset($tekanan) ? $tekanan->rt2 : old('rt2') }}">
                </div>

                <button type="submit" class="btn btn-primary">Simpan</button>

            </form>
        </div>
    </div>
@endsection