<div class="modal modal-default fade" id="detail{{ $tekanan->id }}" data-focus="true" data-backdrop="true" data-keyboard="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="title-form"><b>Detail</b></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label>Waktu</label>
                    <input type="text" class="form-control" readonly
                    value="{{ $tekanan->waktu }}">
                </div>
                <div class="form-group">
                    <label>BMP280_1</label>
                    <input type="text" class="form-control" readonly
                    value="{{ $tekanan->ps1 }}">
                </div>
                <div class="form-group">
                    <label>BMP280_2</label>
                    <input type="text" class="form-control" readonly
                    value="{{ $tekanan->ps2 }}">
                </div>
                <div class="form-group">
                    <label>BMP280_1</label>
                    <input type="text" class="form-control" readonly
                    value="{{ $tekanan->rt1 }}">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary pull-right" data-dismiss="modal">Tutup</button>
            </div>
        </div>
    </div>
</div>