<div class="modal modal-default fade" id="hapus{{$tekanan->id}}" data-focus="true" data-backdrop="true" data-keyboard="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="title-form"><b>Konfirmasi Hapus</b></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <h6>Yakin ingin menghapus data ini?</h6>
                <h6>Data yang telah di hapus tidak akan bisa di pulihkan kembali!</h6>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary pull-right" data-dismiss="modal">Tutup</button>
                <a href="{{ route('tekanan.hapus', ['id' => $tekanan->id]) }}" type="button" class="btn btn-danger pull-right">Hapus</a>
            </div>
        </div>
    </div>
</div>