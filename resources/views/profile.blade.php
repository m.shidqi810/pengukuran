@extends('layouts.app')
@section('judul', 'Edit Profile')
@section('profile', 'active')

@section('isi')
    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-body">
            <form method="POST" id="form-tambah" action="{{ route('profile.update', $akun->id) }}" enctype="multipart/form-data">
                @csrf
                <div class="mb-3">
                    <label for="name" class="form-label">Nama</label>
                    <input type="text" class="form-control" id="name" name="name"
                    value="{{ isset($akun) ? $akun->name : old('akun') }}">
                </div>
                <div class="mb-3">
                    <label for="email" class="form-label">Email</label>
                    <input type="text" class="form-control" id="email" name="email"
                    value="{{ isset($akun) ? $akun->email : old('akun') }}">
                </div>
                <div class="mb-3">
                    <label for="password" class="form-label">Password</label>
                    <input type="password" class="form-control" id="password" name="password">
                    @if (isset($akun))
                        <p>*Kosongkan jika tidak ingin mengubah password</p>
                    @endif
                </div>
                
                <input type="hidden" value="{{ $akun->role == 'Dekanat' ? 'Dekanat' : 'Ketua Program Studi' }}">

                <button type="submit" class="btn btn-primary">Simpan</button>

            </form>
        </div>
    </div>
@endsection