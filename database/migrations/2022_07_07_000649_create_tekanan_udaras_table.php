<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTekananUdarasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tekanan_udaras', function (Blueprint $table) {
            $table->id();
            $table->date('waktu');
            $table->double('ps1');
            $table->double('ps2');
            $table->integer('rt1');
            $table->integer('rt2');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tekanan_udaras');
    }
}
