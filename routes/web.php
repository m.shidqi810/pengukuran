<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\TekananController;
use App\Http\Controllers\AkunController;
use App\Http\Controllers\ProfileController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::middleware(['auth'])->group(function () {

    Route::get('dashboard', [DashboardController::class, 'index'])->name('dashboard');

    Route::prefix('tekanan')->group(function () {
        Route::get('', [TekananController::class, 'index'])->name('tekanan');
        Route::get('tambah', [TekananController::class, 'tambah'])->name('tekanan.tambah');
        Route::post('tambah', [TekananController::class, 'simpan'])->name('tekanan.simpan');
        Route::get('edit/{id}', [TekananController::class, 'edit'])->name('tekanan.edit');
        Route::post('edit/{id}', [TekananController::class, 'update'])->name('tekanan.update');
        Route::get('hapus/{id}', [TekananController::class, 'hapus'])->name('tekanan.hapus');
    });
    
    Route::prefix('about')->group(function () {
        Route::get('', [DataMahasiswaController::class, 'index'])->name('about');
    });

});


require __DIR__.'/auth.php';
