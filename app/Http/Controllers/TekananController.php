<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\TekananUdara;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;

use Alert;

class TekananController extends Controller
{
    public function index()
    {
        $daftar_tekanan = TekananUdara::all();

    	return view('tekanan.index', compact('daftar_tekanan'));
    }

    public function tambah()
    {
        $tekanan          = null;
        
        return view('tekanan.form', compact('tekanan'));
    }
    
    public function edit($id)
    {
        $tekanan          = TekananUdara::where('id', $id)->first();

        return view('tekanan.form', compact('tekanan'));
    }

    public function simpan(Request $request)
    {
        $input = collect($request)->toArray();
        Arr::forget($input, ['_token']);

        TekananUdara::insert($input);

        alert()->success('Success','Data berhasil disimpan');
        return redirect()->route('tekanan');
    }

    public function update(Request $request, $id)
    {
        $input = collect($request)->toArray();
        Arr::forget($input, ['_token']);

        TekananUdara::where('id', $id)->update($input);

        alert()->success('Success','Data berhasil diubah');
        return redirect()->route('tekanan');
    }

    public function hapus($id)
    {
        TekananUdara::where('id', $id)->delete();

        alert()->warning('Success','Data berhasil dihapus');
        return redirect()->route('tekanan');
    }
}
