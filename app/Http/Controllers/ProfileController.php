<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

use App\Models\User;
use Arr;

class ProfileController extends Controller
{
    public function edit($id)
    {
        $akun = User::where('id', $id)->first();
        return view('profile', compact('akun'));
    }

    public function update(Request $request, $id)
    {
        $input = collect($request)->toArray();
        Arr::forget($input, ['_token']);

        if (isset($request->password)) {
            $input['password'] = Hash::make($request->password);
        }
        else {
            Arr::forget($input, ['password']);
        }

        User::where('id', $id)->update($input);

        alert()->success('Success','Data berhasil diubah');
        return redirect()->route('profile', $id);
    }
}
