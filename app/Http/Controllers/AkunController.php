<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

use App\Models\User;
use Arr;

class AkunController extends Controller
{
    public function index()
    {
        $list_user = User::all();
    	return view('akun.index', compact('list_user'));
    }

    public function tambah()
    {
        $akun = null;
        return view('akun.form', compact('akun'));
    }

    public function edit($id)
    {
        $akun = User::where('id', $id)->first();
        return view('akun.form', compact('akun'));
    }

    public function simpan(Request $request)
    {
        $input = collect($request)->toArray();
        Arr::forget($input, ['_token']);

        if (isset($request->password)) {
            $input['password'] = Hash::make($request->password);
        }

        User::insert($input);

        alert()->success('Success','Data berhasil disimpan');
        return redirect()->route('akun');
    }

    public function update(Request $request, $id)
    {
        $input = collect($request)->toArray();
        Arr::forget($input, ['_token']);

        if (isset($request->password)) {
            $input['password'] = Hash::make($request->password);
        }
        else {
            Arr::forget($input, ['password']);
        }

        User::where('id', $id)->update($input);

        alert()->success('Success','Data berhasil diubah');
        return redirect()->route('akun');
    }

    public function hapus($id)
    {
        User::where('id', $id)->delete();

        alert()->warning('Success','Data berhasil dihapus');
        return redirect()->route('akun');
    }
}
