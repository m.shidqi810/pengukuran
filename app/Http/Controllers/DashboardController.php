<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\TekananUdara;

class DashboardController extends Controller
{
    public function index()
    {
        $tekanan = TekananUdara::orderByDesc('id')->first();

    	return view('dashboard', compact('tekanan'));
    }
}
